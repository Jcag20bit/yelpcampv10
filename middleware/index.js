//all the middleware goes here
const Campground = require("../models/campground");
const Comment = require("../models/comment");

let middlewareObj = {};

middlewareObj.checkCampgroundOwnership = function(req, res, next){
            // is user logged in?
            if(req.isAuthenticated()){
                Campground.findById(req.params.id, function(err, foundCampground){
                    if(err){
                        res.redirect("/campgrounds");
                    } else {
                        // does user own the campground?
                        // console.log(foundCampground.author.id);
                        // console.log(req.user._id);
                        if(foundCampground.author.id.equals(req.user._id)) {
                            next();
                        } else {
                            res.redirect("back");  
                        }
                    }
                });
        
            } else {
                res.redirect("back");
            }
    }


middlewareObj.checkCommentOwnership = function(req, res, next){
        // is user logged in?
        if(req.isAuthenticated()){
            Campground.findById(req.params.id, function(err, foundCampground){
                if(err){
                    res.redirect("/campgrounds");
                } else {
                    // does user own the campground?
                    // console.log(foundCampground.author.id);
                    // console.log(req.user._id);
                    if(foundCampground.author.id.equals(req.user._id)) {
                        next();
                    } else {
                        res.redirect("back");  
                    }
                }
            });
    
        } else {
            res.redirect("back");
        }
}

middlewareObj.isLoggedIn = function (req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/login");
};

module.exports = middlewareObj;